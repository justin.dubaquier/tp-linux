# TP2 : Manipulation de services

- [TP2 : Manipulation de services](#tp2--manipulation-de-services)
- [Intro](#intro)
- [Prérequis](#prérequis)
  - [1. Une machine xubuntu fonctionnelle](#1-une-machine-xubuntu-fonctionnelle)
  - [2. Nommer la machine](#2-nommer-la-machine)
  - [3. Config réseau](#3-config-réseau)
- [Go next](#go-next)

Dans nos cours, on ne va que peu s'attarder sur l'aspect client des systèmes GNU/Linux, mais plutôt sur la façon dont on le manipule en tant qu'admin.

Ca permettra aussi, *via* la manipulation, d'appréhender un peu mieux comment un OS de ce genre fonctionne.

# Intro

Dans ce TP on va s'intéresser aux *services* de la machine. Un *service* c'est un processus dont l'OS s'occupe. 

Plutôt que de le lancer à la main, on demande à l'OS de le gérer, c'est moins chiant !

> **Par exemple, quand vous ouvrez votre PC, vous lancez pas une commande pour avoir une interface graphique si ?** L'interface graphique, elle est juste là, elle pop "toute seule". En vérité, c'est l'OS qui l'a lancée. L'interface graphique est donc un *service*.

Souvent un service...

- bon bah c'est un processus qui s'exécute
  - c'est le système qui a fait en sorte qu'il se lance
  - le système a la charge du processus
  - genre il va le relancer si le processus crash par exemple
- souvent il a un fichier de configuration
  - ça nous permet de le paramétrer
  - les changements prennent effet quand on redémarre le service
- souvent on peut définir sous quelle identité le service va tourner
  - genre on désigne un utilisateur du système qui lancera le processus
  - c'est cet utilisateur qui s'affichera dans la liste des processus
- si c'est un service qui utilise le réseau (comme SSH, HTTP, FTP, autres.) il écoute sur un port

# Prérequis

> Y'a toujours une section prérequis dans mes TPs, ça vous sert à préparer l'environnement pour réaliser le TP. Dans ce TP, c'est le premier avec des prérequis, alors je vais détailler le principe et vous devrez me rendre la réalisation de ces étapes dans le compte rendu.

## 1. Une machine xubuntu fonctionnelle

N'hésitez pas à cloner celle qu'on a créé ensemble.

**Pour TOUTES les commandes que je vous donne dans le TP**

- elles sont dans le [memo de commandes Linux](../../cours/memos/commandes.md)
- vous **DEVEZ** consulter le `help` au minimum voire le `man` ou faire une recherche Internet pour comprendre comment fonctionne la commande

```bash
# Consulter le help de ls
$ ls --help

# Consulter le manuel de ls
$ man ls
```

## 2. Nommer la machine

➜ **On va renommer la machine**

- parce qu'on s'y retrouve plus facilement
- parce que toutes les machines sont nommées dans la vie réelle, pour cette raison, alors habituez vous à le faire systématiquement :)

On désignera la machine par le nom `node1.tp2.linux`

🌞 **Changer le nom de la machine**

- **première étape : changer le nom tout de suite, jusqu'à ce qu'on redémarre la machine**
  - taper la commande `sudo hostname <NOM_MACHINE>`

````
jsutin@justi:~$ sudo hostname node1.tp2.linux
justin@node1:~$
````
  - pour vérifier le changement, vous pouvez ouvrir un nouveau terminal, et observer le changement dans le prompt du terminal
- **deuxième étape : changer le nom qui est pris par la machine quand elle s'allume**
  - il faut inscrire le nom de la machine dans le fichier `/etc/hostname`
  - pour vérifier le changement, il faut redémarrer la machine
- je veux les deux étapes dans le compte-rendu

## 3. Config réseau

➜ **Vérifiez avant de continuer le TP que la configuration réseau de la machine est OK. C'est à dire :**

- la machine doit pouvoir joindre internet
- votre PC doit pouvoir `ping` la machine

Pour vérifier que vous avez une configuration réseau correcte (étapes à réaliser DANS LA VM) :

```bash
# Affichez la liste des cartes réseau de la machine virtuelle
# Vérifiez que les cartes réseau ont toute une IP
$ ip a

# Si les cartes n'ont pas d'IP vous pouvez les allumez avec la commande
$ nmcli con up <NOM_INTERFACE>
# Par exemple
$ nmcli con up enp0s3

# Vous devez repérer l'adresse de la VM dans le host-only

# Vous pouvez tester de ping un serveur connu sur internet
# On teste souvent avec 1.1.1.1 (serveur DNS de CloudFlare)
# ou 8.8.8.8 (serveur DNS de Google)
$ ping 1.1.1.1

# On teste si la machine sait résoudre des noms de domaine
$ ping ynov.com
```

Ensuite on vérifie que notre PC peut `ping` la machine (étapes à réaliser SUR VOTRE PC) :

```bash
# Afficher la liste de vos carte réseau, la commande dépend de votre OS
$ ip a # Linux
$ ipconfig # Windows
$ ifconfig # MacOS

# Vous devez repérer l'adresse de votre PC dans le host-only

# Ping de l'adresse de la VM dans le host-only
$ ping <IP_VM>
```

🌞 **Config réseau fonctionnelle**

- dans le compte-rendu, vous devez me montrer...
 - depuis la VM : `ping 1.1.1.1` fonctionnel
````
  justin@node1:~$ ping 1.1.1.1 -c 4
PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.
64 bytes from 1.1.1.1: icmp_seq=1 ttl=56 time=15.9 ms
64 bytes from 1.1.1.1: icmp_seq=2 ttl=56 time=15.3 ms
64 bytes from 1.1.1.1: icmp_seq=3 ttl=56 time=15.0 ms
64 bytes from 1.1.1.1: icmp_seq=4 ttl=56 time=16.2 ms

--- 1.1.1.1 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3005ms
rtt min/avg/max/mdev = 14.978/15.598/16.235/0.499 ms
````
  - depuis la VM : `ping ynov.com` fonctionnel
````
justin@node1:~$ ping ynov.com -c 4
PING ynov.com (92.243.16.143) 56(84) bytes of data.
64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=1 ttl=51 time=14.5 ms
64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=2 ttl=51 time=16.8 ms
64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=3 ttl=51 time=16.4 ms
64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=4 ttl=51 time=15.6 ms

--- ynov.com ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3006ms
rtt min/avg/max/mdev = 14.525/15.822/16.793/0.858 ms
````
  - depuis votre PC : `ping <IP_VM>` fonctionnel
````
PS C:\Users\justi> ping 192.168.251.12

Envoi d’une requête 'Ping'  192.168.251.12 avec 32 octets de données :
Réponse de 192.168.251.12 : octets=32 temps<1ms TTL=64
Réponse de 192.168.251.12 : octets=32 temps<1ms TTL=64
Réponse de 192.168.251.12 : octets=32 temps<1ms TTL=64
Réponse de 192.168.251.12 : octets=32 temps<1ms TTL=64

Statistiques Ping pour 192.168.251.12:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 0ms, Maximum = 0ms, Moyenne = 0ms
````

# Go next

Vous pouvez ensuite vous attaquer aux 3 parties du TP. Elles sont indépendantes mais je vous recommande de les faire dans l'ordre.

> Si ce que je vous demande de faire vous paraît flou, difficile, incompréhensible, si vous savez pas par où commencer ou comment vous y prendre. DEMANDEZ-MOI JE SUIS LA POUR CA. :)

- [Partie 1 : Installation et configuration d'un service SSH](./part1.md)
- [Partie 2 : Installation et configuration d'un service FTP](./part2.md)
- [Partie 3 : Création de votre propre service](./part3.md)

![HF](./pics/have-fun-but-not-too-much.jpg)
# Partie 1 : SSH

- [Partie 1 : SSH](#partie-1--ssh)
- [I. Intro](#i-intro)
- [II. Setup du serveur SSH](#ii-setup-du-serveur-ssh)
  - [1. Installation du serveur](#1-installation-du-serveur)
  - [2. Lancement du service SSH](#2-lancement-du-service-ssh)
  - [3. Etude du service SSH](#3-etude-du-service-ssh)
  - [4. Modification de la configuration du serveur](#4-modification-de-la-configuration-du-serveur)

# I. Intro

> *SSH* c'est pour *Secure SHell*.

***SSH* est un outil qui permet d'accéder au terminal d'une machine, à distance, en connaissant l'adresse IP de cette machine.**

*SSH* repose un principe de *client/serveur* :

- le *serveur*
  - est installé sur une machine par l'admin
  - écoute sur le port 22/TCP par convention
- le *client*
  - connaît l'IP du serveur
  - se connecte au serveur à l'aide d'un programme appelé "*client* *SSH*"

Pour nous, ça va être l'outil parfait pour contrôler toutes nos machines virtuelles.

Dans la vie réelle, *SSH* est systématiquement utilisé pour contrôler des machines à distance. C'est vraiment un outil de routine pour tout informaticien, qu'on utilise au quotidien sans y réfléchir.

> *Si vous louez un serveur en ligne, on vous donnera un accès SSH pour le manipuler la plupart du temps.*

![Feels like a hacker](./pics/feels_like_a_hacker.jpg)

# II. Setup du serveur SSH

Toujours la même routine :

- **1. installation de paquet**
  - avec le gestionnaire de paquet de l'OS
- **2. configuration** dans un fichier de configuration
  - avec un éditeur de texte
  - les fichiers de conf sont de simples fichiers texte
- **3. lancement du service**
  - avec une commande `systemctl start <NOM_SERVICE>`

> ***Pour toutes les commandes tapées qui figurent dans le rendu, je veux la commande ET son résultat. S'il manque l'un des deux, c'est useless.***

## 1. Installation du serveur

Sur les OS GNU/Linux, les installations se font à l'aide d'un gestionnaire de paquets.

🌞 **Installer le paquet `openssh-server`**

- avec une commande `apt install`

````
justin@node1:~$ sudo apt install openssh-server
[sudo] password for justin:
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
openssh-server is already the newest version (1:8.4p1-6ubuntu2).
0 upgraded, 0 newly installed, 0 to remove and 1 not upgraded.
````

---

Note : une fois que le paquet est installé, plusieurs nouvelles choses sont dispos sur la machine. Notamment :

- un service `ssh`
- un dossier de configuration `/etc/ssh/`

## 2. Lancement du service SSH

🌞 **Lancer le service `ssh`**

- avec une commande `systemctl start`
````
justin@node1:~$ systemctl start ssh
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-units ===
Authentication is required to start 'ssh.service'.
Authenticating as: Justin,,, (justin)
Password:
==== AUTHENTICATION COMPLETE ===
````
- vérifier que le service est actuellement actif avec une commande `systemctl status`

````
justin@node1:~$ systemctl status ssh
● ssh.service - OpenBSD Secure Shell server
     Loaded: loaded (/lib/systemd/system/ssh.service; enabled; vendor preset: enabled)
     Active: active (running) since Tue 2021-11-02 17:31:28 CET; 13min ago
       Docs: man:sshd(8)
             man:sshd_config(5)
    Process: 531 ExecStartPre=/usr/sbin/sshd -t (code=exited, status=0/SUCCESS)
   Main PID: 550 (sshd)
      Tasks: 1 (limit: 2314)
     Memory: 3.9M
        CPU: 81ms
     CGroup: /system.slice/ssh.service
             └─550 sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 startups
````

> Vous pouvez aussi faire en sorte que le *service* SSH se lance automatiquement au démarrage avec la commande `systemctl enable ssh`.

## 3. Etude du service SSH

🌞 **Analyser le service en cours de fonctionnement**

- afficher le statut du *service*
  - avec une commande `systemctl status`
````
justin@node1:~$ systemctl status ssh
● ssh.service - OpenBSD Secure Shell server
     Loaded: loaded (/lib/systemd/system/ssh.service; enabled; vendor preset: enabled)
     Active: active (running) since Tue 2021-11-02 17:31:28 CET; 13min ago
       Docs: man:sshd(8)
             man:sshd_config(5)
    Process: 531 ExecStartPre=/usr/sbin/sshd -t (code=exited, status=0/SUCCESS)
   Main PID: 550 (sshd)
      Tasks: 1 (limit: 2314)
     Memory: 3.9M
        CPU: 81ms
     CGroup: /system.slice/ssh.service
             └─550 sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 startups
````

- afficher le/les processus liés au *service* `ssh`
  - avec une commande `ps`
  - isolez uniquement la/les ligne(s) intéressante(s) pour le rendu de TP
````
justin@node1:~$ ps aux
root        1378  0.0  0.4  14948  9112 ?        Ss   13:14   0:00 sshd: justin [priv]
justin      1458  0.0  0.2  14948  6040 ?        S    13:15   0:00 sshd: justin@pts/1
````
- afficher le port utilisé par le *service* `ssh`
  - avec une commande `ss -l`
  - isolez uniquement la/les ligne(s) intéressante(s)
````
justin@node1:~$ ss -l Netid State   Recv-Q   Send-Q   Local   Address:Port   Peer Address:Port   Process
tcp   LISTEN  0       128    0.0.0.0:ssh                     
````
- afficher les logs du *service* `ssh`
  - avec une commande `journalctl`
  - en consultant un dossier dans `/var/log/`
  - ne me donnez pas toutes les lignes de logs, je veux simplement que vous appreniez à consulter les logs

````
justin@node1:/$ journalctl -xe -u ssh
░░
░░ The job identifier is 127.
nov. 02 17:31:28 node1.tp2.linux sshd[550]: Server listening on 0.0.0.0 port 22.
nov. 02 17:31:28 node1.tp2.linux sshd[550]: Server listening on :: port 22.
nov. 02 17:31:28 node1.tp2.linux systemd[1]: Started OpenBSD Secure Shell server.
````

---

🌞 **Connectez vous au serveur**

- depuis votre PC, en utilisant un **client SSH**

> *La commande `ssh` de votre terminal, c'est un client SSH.*

````
PS C:\Users\justi> ssh justin@192.168.251.12
justin@192.168.251.12's password:
Welcome to Ubuntu 21.10 (GNU/Linux 5.13.0-19-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

1 update can be applied immediately.
1 of these updates is a standard security update.
To see these additional updates run: apt list --upgradable


The list of available updates is more than a week old.
To check for new updates run: sudo apt update
Last login: Tue Nov  2 17:34:31 2021 from 192.168.251.1
justin@node1:~$
````

## 4. Modification de la configuration du serveur

Pour modifier comment un *service* se comporte il faut modifier le fichier de configuration. On peut tout changer à notre guise.

🌞 **Modifier le comportement du service**

- c'est dans le fichier `/etc/ssh/sshd_config`
  - c'est un simple fichier texte
  - modifiez-le comme vous voulez, je vous conseille d'utiliser `nano` en ligne de commande
````justin@node1:~$ sudo nano /etc/ssh/sshd_config
sudo: unable to resolve host node1.tp2.linux: Temporary failure in name resolution
[sudo] password for justin:
````
- effectuez le modifications suivante :
  - changer le ***port d'écoute*** du service *SSH*
    - peu importe lequel, il doit être compris entre 1025 et 65536
  - vous me prouverez avec un `cat` que vous avez bien modifié cette ligne
````
justin@node1:~$ cat /etc/ssh/sshd_config
#       $OpenBSD: sshd_config,v 1.103 2018/04/09 20:41:22 tj Exp $

# This is the sshd server system-wide configuration file.  See
# sshd_config(5) for more information.

# This sshd was compiled with PATH=/usr/bin:/bin:/usr/sbin:/sbin

# The strategy used for options in the default sshd_config shipped with
# OpenSSH is to specify options with their default value where
# possible, but leave them commented.  Uncommented options override the
# default value.

Include /etc/ssh/sshd_config.d/*.conf

#Port 33000
[...]
````
- pour cette modification, prouver à l'aide d'une commande qu'elle a bien pris effet
  - une commande `ss -l` pour vérifier le port d'écoute
````
justin@node1:~$ sudo ss -ltpn
[sudo] password for justin:
State    Recv-Q   Send-Q     Local Address:Port        Peer Address:Port   Process
LISTEN   0        128              0.0.0.0:33000            0.0.0.0:*       users:(("sshd",pid=1484,fd=3))
LISTEN   0        128                 [::]:33000               [::]:*       users:(("sshd",pid=1484,fd=4))
````

> Vous devez redémarrer le service avec une commande `systemctl restart` pour que les changements inscrits dans le fichier de configuration prennent effet.

🌞 **Connectez vous sur le nouveau port choisi**

- depuis votre PC, avec un *client SSH*

![When she tells you](./pics/when_she_tells_you.png)

# Partie 2 : FTP

- [Partie 2 : FTP](#partie-2--ftp)
- [I. Intro](#i-intro)
- [II. Setup du serveur FTP](#ii-setup-du-serveur-ftp)
  - [1. Installation du serveur](#1-installation-du-serveur)
  - [2. Lancement du service FTP](#2-lancement-du-service-ftp)
  - [3. Etude du service FTP](#3-etude-du-service-ftp)
  - [4. Modification de la configuration du serveur](#4-modification-de-la-configuration-du-serveur)

# I. Intro

> *FTP* c'est pour *File Transfer Protocol*.

***FTP* est un protocole qui permet d'envoyer simplement des fichiers sur un serveur à travers le réseau.**

*FTP* repose un principe de client/serveur :

- le *serveur*
  - est installé sur une machine par l'admin
  - écoute sur le port 21/TCP par convention
- le *client*
  - connaît l'IP du *serveur*
  - se connecte au *serveur* à l'aide d'un programme appelé "*client FTP*"

Dans la vie réelle, *FTP* est souvent utilisé pour échanger des fichiers avec un serveur de façon sécurisée. En vrai ça commence à devenir oldschool *FTP*, mais c'est un truc très basique et toujours très utilisé.

> Si vous louez un serveur en ligne, on vous donnera parfois un accès *FTP* pour y déposer des fichiers.

![FTP loooong time](./pics/ftp_long_time.jpg)

# II. Setup du serveur FTP

Toujours la même routine :

- **1. installation de paquet**
  - avec le gestionnaire de paquet de l'OS
- **2. configuration** dans un fichier de configuration
  - avec un éditeur de texte
  - les fichiers de conf sont de simples fichiers texte
- **3. lancement du service**
  - avec une commande `systemctl start <NOM_SERVICE>`

> ***Pour toutes les commandes tapées qui figurent dans le rendu, je veux la commande ET son résultat. S'il manque l'un des deux, c'est useless.***

## 1. Installation du serveur

🌞 **Installer le paquet `vsftpd`**

````
justin@node1:~$ sudo apt install vsftpd
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
The following NEW packages will be installed:
  vsftpd
0 upgraded, 1 newly installed, 0 to remove and 1 not upgraded.
Need to get 122 kB of archives.
After this operation, 322 kB of additional disk space will be used.
Get:1 http://fr.archive.ubuntu.com/ubuntu impish/main amd64 vsftpd amd64 3.0.3-13build1 [122 kB]
Fetched 122 kB in 10s (12,0 kB/s)
Preconfiguring packages ...
Selecting previously unselected package vsftpd.
(Reading database ... 160422 files and directories currently installed.)
Preparing to unpack .../vsftpd_3.0.3-13build1_amd64.deb ...
Unpacking vsftpd (3.0.3-13build1) ...
Setting up vsftpd (3.0.3-13build1) ...
Created symlink /etc/systemd/system/multi-user.target.wants/vsftpd.service → /lib/systemd/system/vsftpd.service.
Processing triggers for man-db (2.9.4-2) ...
````

---

Note : une fois que le paquet est installé, plusieurs nouvelles choses sont dispos sur la machine. Notamment :

- un service `vsftpd`
- un fichier de configuration `/etc/vsftpd.conf`

> Le paquet s'appelle `vsftpd` pour *Very Secure FTP Daemon*. A l'apogée de l'utilisation de FTP, il n'était pas réputé pour être un protocole très sécurisé. Aujourd'hui, avec des outils comme `vsftpd`, c'est bien mieux qu'à l'époque.

## 2. Lancement du service FTP

🌞 **Lancer le service `vsftpd`**

- avec une commande `systemctl start`
- vérifier que le service est actuellement actif avec une commande `systemctl status`

````
justin@node1:~$ sudo systemctl start vsftpd
justin@node1:~$ sudo systemctl status vsftpd
● vsftpd.service - vsftpd FTP server
     Loaded: loaded (/lib/systemd/system/vsftpd.service; enabled; vendor preset: enabled)
     Active: active (running) since Sun 2021-11-07 15:18:25 CET; 2min 51s ago
    Process: 1791 ExecStartPre=/bin/mkdir -p /var/run/vsftpd/empty (code=exited, status=0/SUCCESS)
   Main PID: 1792 (vsftpd)
      Tasks: 1 (limit: 2314)
     Memory: 684.0K
        CPU: 3ms
     CGroup: /system.slice/vsftpd.service
             └─1792 /usr/sbin/vsftpd /etc/vsftpd.conf

nov. 07 15:18:25 node1 systemd[1]: Starting vsftpd FTP server...
nov. 07 15:18:25 node1 systemd[1]: Started vsftpd FTP server.
````

> Vous pouvez aussi faire en sorte que le service FTP se lance automatiquement au démarrage avec la commande `systemctl enable vsftpd`.

## 3. Etude du service FTP

🌞 **Analyser le service en cours de fonctionnement**

- afficher le statut du service
  - avec une commande `systemctl status`
````
justin@node1:~$ sudo systemctl status vsftpd
● vsftpd.service - vsftpd FTP server
     Loaded: loaded (/lib/systemd/system/vsftpd.service; enabled; vendor preset: enabled)
     Active: active (running) since Sun 2021-11-07 15:18:25 CET; 2min 51s ago
    Process: 1791 ExecStartPre=/bin/mkdir -p /var/run/vsftpd/empty (code=exited, status=0/SUCCESS)
   Main PID: 1792 (vsftpd)
      Tasks: 1 (limit: 2314)
     Memory: 684.0K
        CPU: 3ms
     CGroup: /system.slice/vsftpd.service
             └─1792 /usr/sbin/vsftpd /etc/vsftpd.conf

nov. 07 15:18:25 node1 systemd[1]: Starting vsftpd FTP server...
nov. 07 15:18:25 node1 systemd[1]: Started vsftpd FTP server.
````
- afficher le/les processus liés au service `vsftpd`
  - avec une commande `ps`
  - isolez uniquement la/les ligne(s) intéressante(s) pour le rendu de TP

````
justin@node1:~$ ps aux
USER         PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
root        1792  0.0  0.1   8668  3812 ?        Ss   15:45   0:00 /usr/sbin/vsftpd /etc/vsftpd.conf
````
- afficher le port utilisé par le service `vsftpd`
  - avec une commande `ss -l`
  - isolez uniquement la/les ligne(s) intéressante(s)

````
justin@node1:~$ sudo ss -ltpn
State                   Recv-Q                  Send-Q                                   Local Address:Port                                    Peer Address:Port                 Process

LISTEN                  0                       32                                                   *:21                                                 *:*                     users:(("vsftpd",pid=1792,fd=3))

````

- afficher les logs du service `vsftpd`
  - avec une commande `journalctl`
  - en consultant un fichier dans `/var/log/`
  - ne me donnez pas toutes les lignes de logs, je veux simplement que vous appreniez à consulter les logs

````
justin@node1:~$ journalctl |grep vsftpd
nov. 07 15:18:12 justin-vm sudo[1639]:   justin : TTY=pts/2 ; PWD=/home/justin ; USER=root ; COMMAND=/usr/bin/apt install vsftpd
nov. 07 15:18:25 justin-vm systemd[1]: Starting vsftpd FTP server...
nov. 07 15:18:25 justin-vm systemd[1]: Started vsftpd FTP server.
nov. 07 15:20:55 justin-vm sudo[17003]:   justin : TTY=pts/2 ; PWD=/home/justin ; USER=root ; COMMAND=/usr/bin/systemctl vsftpd start
nov. 07 15:21:07 justin-vm sudo[17005]:   justin : TTY=pts/2 ; PWD=/home/justin ; USER=root ; COMMAND=/usr/bin/systemctl start vsftpd
nov. 07 15:21:16 justin-vm sudo[17008]:   justin : TTY=pts/2 ; PWD=/home/justin ; USER=root ; COMMAND=/usr/bin/systemctl status vsftpd
nov. 07 15:37:36 justin-vm sudo[17041]:   justin : TTY=pts/2 ; PWD=/home/justin ; USER=root ; COMMAND=/usr/bin/systemctl status vsftpd
````

---

🌞 **Connectez vous au serveur**

- depuis votre PC, en utilisant un *client FTP*
  - les navigateurs Web, ils font ça maintenant
  - demandez moi si vous êtes perdus
- essayez d'uploader et de télécharger un fichier
  - montrez moi à l'aide d'une commande la ligne de log pour l'upload, et la ligne de log pour le download
- vérifier que l'upload fonctionne
  - une fois un fichier upload, vérifiez avec un `ls` sur la machine Linux que le fichier a bien été uploadé

🌞 **Visualiser les logs**

- mettez en évidence une ligne de log pour un download
- mettez en évidence une ligne de log pour un upload

````
justin@node1:~$ sudo cat /var/log/vsftpd.log
Sun Nov  7 23:14:49 2021 [pid 18439] [justin] OK UPLOAD: Client "::ffff:192.168.251.1", "/home/justin/Documents/testtp.txt.txt", 4 bytes, 6.40Kbyte/sec
Sun Nov  7 23:17:21 2021 [pid 18475] [justin] OK DOWNLOAD: Client "::ffff:192.168.251.1", "/home/justin/Documents/testtp.txt.txt", 4 bytes, 7.25Kbyte/sec
````

## 4. Modification de la configuration du serveur

Pour modifier comment un service se comporte il faut modifier de configuration. On peut tout changer à notre guise.

🌞 **Modifier le comportement du service**

- c'est dans le fichier `/etc/vsftpd.conf`
- effectuez les modifications suivantes :
  - changer le port où écoute `vstfpd`
    - peu importe lequel, il doit être compris entre 1025 et 65536
  - vous me prouverez avec un `cat` que vous avez bien modifié cette ligne

````
justin@justin-vm:~$ sudo cat /etc/vsftpd.conf
# Example config file /etc/vsftpd.conf
#
# The default compiled in settings are fairly paranoid. This sample file
# loosens things up a bit, to make the ftp daemon more usable.
# Please see vsftpd.conf.5 for all compiled in defaults.
#
# READ THIS: This example file is NOT an exhaustive list of vsftpd options.
# Please read the vsftpd.conf.5 manual page to get a full idea of vsftpd's
# capabilities.
listen_port=33210
[...]
````
- pour les deux modifications, prouver à l'aide d'une commande qu'elles ont bien pris effet
  - une commande `ss -l` pour vérifier le port d'écoute

````
justin@node1:~$ sudo ss -ltpn
State    Recv-Q   Send-Q     Local Address:Port      Peer Address:Port  Process
LISTEN   0        32                     *:33210                *:*      users:(("vsftpd",pid=18493,fd=3))
````

> Vous devez redémarrer le service avec une commande `systemctl restart` pour que les changements inscrits dans le fichier de configuration prennent effet.

🌞 **Connectez vous sur le nouveau port choisi**

- depuis votre PC, avec un *client FTP*
- re-tester l'upload et le download

````
justin@node1:~$ sudo cat /var/log/vsftpd.log
Sun Nov  7 23:26:43 2021 [pid 18513] [justin] OK UPLOAD: Client "::ffff:192.168.251.1", "/home/justin/testtp.txt.txt", 4 bytes, 1.25Kbyte/sec
Sun Nov  7 23:26:52 2021 [pid 18513] [justin] OK DOWNLOAD: Client "::ffff:192.168.251.1", "/home/justin/testtp.txt.txt", 4 bytes, 4.41Kbyte/sec

````

![Suuuuuuuuuuuuuure](./pics/files-on-ftp-suuuure-any-minute-now.jpg)

# Partie 3 : Création de votre propre service

- [Partie 3 : Création de votre propre service](#partie-3--création-de-votre-propre-service)
- [I. Intro](#i-intro)
- [II. Jouer avec netcat](#ii-jouer-avec-netcat)
- [III. Un service basé sur netcat](#iii-un-service-basé-sur-netcat)
  - [1. Créer le service](#1-créer-le-service)
  - [2. Test test et retest](#2-test-test-et-retest)

# I. Intro

Comme on l'a dit plusieurs fois plus tôt, un *service* c'est juste un processus que l'on demande au système de lancer. Puis il s'en occupe.

Ainsi, il nous faut juste trouver comment, dans Linux, on fait pour définir un nouveau *service*. On aura plus qu'à indiquer quel processus on veut lancer.

Histoire d'avoir un truc un minimum tangible, et pas juste un service complètement inutile, on va apprendre un peu à utiliser `netcat` avant de continuer.

`netcat` est une commande très simpliste qui permet deux choses :

- **écouter sur un port** réseau, et attendre la connexion de clients
  - on parle alors d'un `netcat` qui agit comme un serveur
- **se connecter sur un port** d'un serveur dont on connaît l'IP
  - on parle alors d'un `netcat` qui agit comme un client

Dans un premier temps, vous allez utiliser `netcat` à la main et jouer un peu avec. On peut fabriquer un outil de discussion, un chat, assez facilement avec `netcat`. Un chat entre deux machines connectées sur le réseau !

Ensuite, vous créerez un service basé sur `netcat` qui permettra d'écrire dans un fichier de la machine, à distance.

> ***Pour toutes les commandes tapées qui figurent dans le rendu, je veux la commande ET son résultat. S'il manque l'un des deux, c'est useless.***

# II. Jouer avec netcat

Si vous avez compris la partie I avec SSH et la partie II avec FTP, c'est toujours le même principe : un serveur qui attend des connexions, et un client qui s'y connecte. Le principe :

- la VM va agir comme un serveur, à l'aide de la commande `netcat`
  - ce sera une commande `nc -l`
  - le `-l` c'est pour `listen` : le serveur va écouter
  - il faudra préciser un port sur lequel écouter
- votre PC agira comme le client
  - il faudra avoir la commande `nc` dans le terminal de votre PC
  - ce sera une commande `nc` (sans le `-l`) pour se connecter à un serveur
  - il faudra préciser l'IP et le port où vous voulez vous connecter (IP et port de la VM)

> Sur Windows, la commande s'appelle souvent `ncat.exe` ou `nc.exe`.

![Install Net Cat hihi](./pics/install_netcat.jpg)

Une fois la connexion établie, vous devrez pouvoir échanger des messages entre les deux machines, comme un petit chat !

🌞 **Donnez les deux commandes pour établir ce petit chat avec `netcat`**

- la commande tapée sur la VM

````
justin@node1:~$ nc -l 10000
````
- la commande tapée sur votre PC

````
justin@justin-vm-netcat:~$ nc 192.168.251.13 10000
````


🌞 **Utiliser `netcat` pour stocker les données échangées dans un fichier**

- utiliser le caractère `>` et/ou `>>` sur la ligne de commande `netcat` de la VM
- cela permettra de stocker les données échangées dans un fichier
- plutôt que de les afficher dans le terminal
- parce quuueeee pourquoi pas ! Ca permet de faire d'autres trucs avec

````
justin@node1:~$ nc -l 10000 > test.txt

justin@node1:~$ cat test.txt
bonjour
````

Le caractère `>` s'utilise comme suit :

```bash
# On liste les fichiers et dossiers
$ ls
Documents Images

# Le caractère > permet de rediriger le texte dans un fichier
# Plutôt que de l'afficher dans un terminal
$ ls > toto
# Notez l'absence de texte en retour de la commande ls

# Voyons le résultat d'une simple commande ls désormais :
$ ls
Documents Images toto
# Un nouveau fichier toto a été créé

# Regardons son contenu
$ cat toto
Document Images
# Il contient du texte : le résultat de la commande ls
```

> N'hésitez pas à vous entraîner à utiliser `>` et `>>` sur la ligne de commande avant de vous lancer dans cette partie. Je vous laisse Google pour voir la différence entre les deux.

Il sera donc possible de l'utiliser avec `netcat` comme suit :

```bash
$ nc -l IP PORT > YOUR_FILE
```

Ainsi, le fichier `YOUR_FILE` contiendra tout ce que le client aura envoyé vers le serveur avec `netcat`, plutôt que ça s'affiche juste dans le terminal.

> Renommez le fichier `YOUR_FILE` comme vous l'entendez évidemment.

# III. Un service basé sur netcat

**Pour créer un service sous Linux, il suffit de créer un simple fichier texte.**

Ce fichier texte :

- a une syntaxe particulière
- doit se trouver dans un dossier spécifique

Pour essayer de voir un peu la syntaxe, vous pouvez utilisez la commande `systemctl cat` sur un service existant. Par exemple `systemctl cat sshd`.

DON'T PANIC pour votre premier service j'vais vous tenir la main.

La commande que lancera votre service sera un `nc -l` : vous allez donc créer un petit chat sous forme de service ! Ou presque hehe.

## 1. Créer le service

🌞 **Créer un nouveau service**

- créer le fichier `/etc/systemd/system/chat_tp2.service`

````
justin@node1:/etc/systemd/system$ sudo nano chat_tp2.service
````
- définissez des permissions identiques à celles des aux autres fichiers du même type qui l'entourent

````
justin@node1: sudo chmod 777 /etc/systemd/system/chat_tp2.service
````
- déposez-y le contenu suivant :

```bash
[Unit]
Description=Little chat service (TP2)

[Service]
ExecStart=<NETCAT_COMMAND>

[Install]
WantedBy=multi-user.target
```

Vous devrez remplacer `<NETCAT_COMMAND>` par une commande `nc` de votre choix : 

````
justin@node1:/etc/systemd/system$ cat chat_tp2.service
[Unit]
Description=Little chat service (TP2)

[Service]
ExecStart=nc -l 10000

[Install]
WantedBy=multi-user.target
````

- `nc` doit écouter, listen (`-l`)
- et vous devez préciser le chemin absolu vers cette commande `nc`
  - vous pouvez taper la commande `which nc` pour connaître le dossier où se trouve `nc` (son chemin absolu)

**Il faudra exécuter la commande `sudo systemctl daemon-reload` à chaque fois que vous modifiez un fichier `.service`.**

## 2. Test test et retest

🌞 **Tester le nouveau service**

- depuis la VM
  - démarrer le nouveau service avec une commande `systemctl start`

````
justin@node1:/$ sudo systemctl start chat_tp2
````
  - vérifier qu'il est correctement lancé avec une commande  `systemctl status`
````
justin@node1:/$ sudo systemctl status chat_tp2
● chat_tp2.service - Little chat service (TP2)
     Loaded: loaded (/etc/systemd/system/chat_tp2.service; disabled; vendor preset: enabled)
     Active: active (running) since Mon 2021-11-08 00:10:42 CET; 9s ago
   Main PID: 18861 (nc)
      Tasks: 1 (limit: 2314)
     Memory: 212.0K
        CPU: 1ms
     CGroup: /system.slice/chat_tp2.service
             └─18861 nc -l 10000

nov. 08 00:10:42 justin-vm systemd[1]: Started Little chat service (TP2).
````

  - vérifier avec une comande `ss -l` qu'il écoute bien derrière le port que vous avez choisi

````
justin@node1:/$ sudo ss -ltpn
State    Recv-Q   Send-Q     Local Address:Port        Peer Address:Port   Process
LISTEN   0        1                0.0.0.0:10000            0.0.0.0:*       users:(("nc",pid=18861,fd=3))
````
- tester depuis votre PC que vous pouvez vous y connecter
- pour visualiser les messages envoyés par le client, il va falloir regarder les logs de votre service, sur la VM :

```bash
# Voir l'état du service, et les derniers logs
$ systemctl status chat_tp2

# Voir tous les logs du service
$ journalctl -xe -u chat_tp2

# Suivre en temps réel l'arrivée de nouveaux logs
# -f comme follow :)
$ journalctl -xe -u chat_tp2 -f
```

````
justin@node1:/$ journalctl -xe -u chat_tp2 -f
-- Journal begins at Tue 2021-10-19 11:26:03 CEST. --
nov. 08 00:10:42 justin-vm systemd[1]: Started Little chat service (TP2).
░░ Subject: A start job for unit chat_tp2.service has finished successfully
░░ Defined-By: systemd
░░ Support: http://www.ubuntu.com/support
░░
░░ A start job for unit chat_tp2.service has finished successfully.
░░
░░ The job identifier is 10991.
nov. 08 00:15:52 justin-vm nc[18861]: test
````