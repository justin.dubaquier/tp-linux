# TP1 : Are you dead yet ?

**Le but de ce TP est de trouver au moins 5 façons différentes de péter la machine.**

---

**Je vais donc lister les differentes façon que j'ai trouver des casser la machine :**

---

**Première Solution :** supprimer le fichier bin 

Le but est de **supprimer** des fichier essentiel au bon fonctionnement de la machine
 pour cela j'ai utiliser deux commandes differentes : 
 
 ``` cd .. ``` et ``` sudo rm -rf bin/ ```
 
 j'ai utilisé ``` cd .. ``` pour aller à la base de l'arboressence des fichiers de la machine et une fois arriver j'ai uttiliser ``` sudo rm -rf bin/ ``` ce qui a tout simplemet supprimer le fichier **bin** 
 
 A partir de se moment la plus qu'à essayer de **restart** la machine pour se rendre compte qu'il n'est plus possible de **boot**
 
 ---
 
 **Deuxième Solution :** l'extinction automatique

Le but est que la machine **s'éteigne** une fois que l'utilisateur se **log** 
 pour cela j'ai uttiliser deux commandes differentes : 
 
 ``` cd ``` et ``` sudo nano tu_ne_t_alumeras_pas.sh```
 
 j'ai utilisé ``` cd .. ``` et ``` cd etc/ ``` pour aller à la base de l'arboressence puis je suis aller dans le dossier **etc** une fois arriver j'ai utiliser ``` sudo nano tu_ne_t_alumeras_pas.sh``` afin d'ouvrir un editeur de texte dans lequel j'ai taper ``` #!bin/bash
                shutdown now``` 

ensuite je suis aller dans le menu startup et dans la categirie 
 
 A partir de se moment la plus qu'à essayer de **restart** la machine a la possibilité de **boot** mais elle se met hors tention des que l'utilisateur se **log**
 
 ---
 
 **Troisième Solution :** Suppretion d'acces à des fichiers importants

Le but est que la machine ne puisse plus **boot**. 
 pour cela j'ai uttiliser une seule commande : 
 
 ``` sudo chmod 000 /lib/systemd/systemd```
 
 j'ai tout simplement utiliser la commande 
 ``` sudo chmod 000 /lib/systemd/systemd```  ce qui a pour but de retirer les permission d'acces aux fichers **systemd** qui sont essentiels au **boot** de la machine 
 
 A partir de se moment la plus qu'à essayer de **restart** la machine pour se rendre compte que il n'est plus possible de **boot**
 
 ---
 
  **Quatrième Solution :** Surcharge de la RAM 

Le but est que la machine ne puisse plus utiliser la **RAM**. 
 pour cela j'ai uttiliser une seule commande : 
 
 ``` cd ``` et ``` sudo nano la_ram_tu_n_utiliseras_pas.sh```
 
  j'ai utilisé ``` cd .. ``` et ``` cd etc/ ``` pour aller à la base de l'arboressence puis je suis aller dans le dossier **etc** une fois arriver j'ai utiliser ``` sudo nano tu_ne_t_alumeras_pas.sh``` afin d'ouvrir un éditeur de texte dans lequel j'ai taper ``` #!bin/bash
                :(){ :|:& };:``` 
ce qui sert a appeler une fonction récurcive qui s'apelle elle même en boucle 

ensuite je suis aller dans le menu startup et j'ai ajouter mon script au fichier à executer au demarage de la machine
 
 A partir de se moment la plus qu'à essayer de **restart** la machine pour se rendre compte que il est possible de **boot** mains la machine est tres lente et quand l'utilisateur essaie d'executer une application apres un long moment de chargement un message d'erreur s'affiche en signalant qu'il n'est pas possible d'executer l'application car la **RAM** dispo n'est pas suffisante 
 et la machine finiera pas **planter**.
 
 ---
 **Cinquième Solution :** la desintegration

Le but est que l'utilisateur supprine tout les ficher de la machine 
 Le but est que la machine ne puisse plus **boot**. 
 pour cela j'ai uttiliser une seule commande : 
 
 ``` sudo rm -r ./*```
 
 j'ai tout simplement utiliser la commande 
 ``` sudo rm -r ./*```  ce qui a pour but de **supprimer** la totalité des fichier présent sur le disque de la machine
 
 A partir de se moment la les fichiers se supprime un par un et une fois qu'ils ont tous été effacé on arrive sur un écran noir ou on ne peu plus rien faire...
 
 ---
 

