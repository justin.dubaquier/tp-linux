# TP4 : Une distribution orientée serveur

# II. Checklist

![Checklist is here !](./pics/checklist_is_here.jpg)

A chaque fois que vous créerez une machine Rocky Linux, vous vérifierez les éléments de la checklist qui suit.

Pour cette première fois, vous me rendrez dans le compte-rendu les étapes que vous avez réalisé pour mener ça à bien. Dans les futurs TPs, la checklist devra simplement être réalisée à chaque fois.

**Créez donc une nouvelle machine** (clonez celle qu'on vient d'install). On l'appellera `node1.tp4.linux`.

---

➜ **Configuration IP statique**

Jusqu'à maintenant on laissait notre machine récupérer une IP automatiquement. Pour des clients, c'est ce qui se passe dans la vie réelle (genre ton smartphone connecté à ta box, il récup une IP automatiquement).

**Pour un serveur, c'est différent : on veut que son IP soit prévisible, donc fixe. On la définit donc à la main.**

Dans Rockly Linux, on définit la configuration des cartes réseau dans le dossier `/etc/sysconfig/network-scripts/`. Il existe là-bas (on peut le créer s'il n'existe pas), un fichier par interface réseau.

Par exemple, pour l'interface `enp0s8`, le fichier de configuration se nommera `ifcfg-enp0s8`, au chemin`/etc/sysconfig/network-scripts/ifcfg-enp0s8`.

Le contenu que vous devez utiliser :

```
NAME=enp0s8          # nom de l'interface
DEVICE=enp0s8        # nom de l'interface
BOOTPROTO=static     # définition statique de l'IP (par opposition à DHCP)
ONBOOT=yes           # la carte s'allumera automatiquement au boot de la machine
IPADDR=<IP_CHOISIE>  # adresse IP choisie
NETMASK=<MASK>       # masque choisi
```

A chaque fois que vous modifiez un fichier de configuration d'interface, il faudra exécuter :

```bash
# on indique au système que les fichiers ont été modifiés
$ sudo nmcli con reload

# on allume/reload l'interface avec la nouvelle conf
$ sudo nmcli con up <INTERFACE>
# par exemple
$ sudo nmcli con up enp0s8
```

> Ceci est aussi mentionné dans [le mémo réseau Rocky Linux](../../cours/memos/rocky_network.md)

🌞 **Choisissez et définissez une IP à la VM**

- vous allez devoir configurer l'interface host-only
- ce sera nécessaire pour pouvoir SSH dans la VM et donc écrire le compte-rendu
- je veux, dans le compte-rendu, le contenu de votre fichier de conf, et le résultat d'un `ip a` pour me prouver que les changements on pris effet

````
[justin@localhost ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s8cripts/ifcfg-enp0s8
TYPE=Ethernet
BOOTPROTO=static
NAME=enp0s8
DEVICE=enp0s8
ONBOOT=yes
IPADDR=10.200.1.10
NETMASK=255.255.255.0


[justin@localhost ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:a6:5e:a3 brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
       valid_lft 83960sec preferred_lft 83960sec
    inet6 fe80::a00:27ff:fea6:5ea3/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:df:a6:2b brd ff:ff:ff:ff:ff:ff
    inet 10.200.1.10/24 brd 10.200.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fedf:a62b/64 scope link
       valid_lft forever preferred_lft forever
````


---

➜ **Connexion SSH fonctionnelle**

Vous pouvez vous connecter en SSH à la VM. Cela implique :

- la VM a une carte réseau avec une IP locale
- votre PC peut joindre cette IP
- la VM a un serveur SSH qui est accessible derrière l'un des ports réseau

🌞 **Vous me prouverez que :**

- le service ssh est actif sur la VM
  - avec une commande `systemctl status ...`

````
[justin@localhost ~]$ sudo systemctl status sshd
● sshd.service - OpenSSH server daemon
   Loaded: loaded (/usr/lib/systemd/system/sshd.service; enabled; vendor preset: enabled)
   Active: active (running) since Wed 2021-11-24 15:19:31 CET; 3min 40s ago
     Docs: man:sshd(8)
           man:sshd_config(5)
 Main PID: 2149 (sshd)
    Tasks: 1 (limit: 4946)
   Memory: 2.8M
   CGroup: /system.slice/sshd.service
           └─2149 /usr/sbin/sshd -D -oCiphers=aes256-gcm@openssh.com,chacha20-poly1305@openssh.com,aes256-ctr,aes256-cb>

````
- vous pouvez vous connecter à la VM, grâce à un échange de clés
  - référez-vous [au cours sur SSH pour + de détails sur l'échange de clés](../../cours/cours/SSH/README.md)

> Pour me prouver que vous pouvez vous connecter avec un échange de clé il faut me montrer, dans l'idéal : la clé publique (le cadenas) sur votre PC (un `cat` du fichier), un `cat` du fichier `authorized_keys` concerné sur la VM, et une connexion sans aucun mot de passe demandé.

---

➜ **Accès internet**

Par l'expression commune et barbare "avoir un accès internet" on entend deux choses généralement :

- *ahem* avoir un accès internet
  - c'est à dire être en mesure de ping des IP publiques
- avoir de la résolution de noms
  - la machine doit pouvoir traduire un nom comme `google.com` vers l'IP associée

🌞 **Prouvez que vous avez un accès internet**

- avec une commande `ping`

> On utilise souvent un `ping` vers une adresse IP publique connue pour tester l'accès internet.  
Vous verrez souvent `8.8.8.8` c'est l'adresse d'un serveur Google. On part du principe que Google sera bien le dernier à quitter Internet, donc on teste l'accès internet en le pingant.  
Si vous n'aimez pas Google comme moi, vous pouvez `ping 1.1.1.1`, c'est les serveurs de CloudFlare.

````
[justin@localhost ~]$ ping 8.8.8.8 -c4
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=25.9 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=113 time=14.9 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=113 time=14.6 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=113 time=13.4 ms

--- 8.8.8.8 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3004ms
rtt min/avg/max/mdev = 13.384/17.213/25.946/5.074 ms
````

🌞 **Prouvez que vous avez de la résolution de nom**

Un petit `ping` vers un nom de domaine, celui que vous voulez :)

````
[justin@localhost ~]$ ping youtube.com -c4
PING youtube.com (216.58.215.46) 56(84) bytes of data.
64 bytes from par21s17-in-f14.1e100.net (216.58.215.46): icmp_seq=1 ttl=113 time=14.1 ms
64 bytes from par21s17-in-f14.1e100.net (216.58.215.46): icmp_seq=2 ttl=113 time=15.3 ms
64 bytes from par21s17-in-f14.1e100.net (216.58.215.46): icmp_seq=3 ttl=113 time=12.5 ms
64 bytes from par21s17-in-f14.1e100.net (216.58.215.46): icmp_seq=4 ttl=113 time=14.4 ms

--- youtube.com ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3005ms
rtt min/avg/max/mdev = 12.546/14.066/15.271/0.989 ms
````
---

➜ **Nommage de la machine**

Votre chambre vous la rangez na ? Au moins de temps en temps ? Là c'est pareil : on range les machines. Et ça commence par leur donner un nom.

Pour donner un nom à une machine il faut exécuter deux actions :

- changer son nom immédiatement, mais le changement sera perdu au reboot
- écrire son nom dans un fichier, pour que le fichier soit lu au boot et que le changement persiste

En commande ça donne :

```bash
# Pour la session
$ sudo hostname <NOM>

# Persistant après les reboots
$ sudo nano /etc/hostname # remplacer le contenu du fichier par le NOM
```

🌞 **Définissez `node1.tp4.linux` comme nom à la machine**

- montrez moi le contenu du fichier `/etc/hostname`

````
[justin@localhost ~]$ cat /etc/hostname
node1.tp4.linux
````
- tapez la commande `hostname` (sans argument ni option) pour afficher votre hostname actuel

````
[justin@localhost ~]$ hostname
node1.tp4.linux
````
> Vous verrez aussi votre hostname dans le prompt du terminal, en plus de l'utilisateur avec lequel vous êtes co : `it4@node1:~$`.  
C'est de l'anglais en fait hein, le "@" se dit "at". Donc en l'occurence "it4 at node1".

## 2. Install

🌞 **Installez NGINX en vous référant à des docs online**

````
[justin@localhost ~]$ sudo dnf install nginx
Last metadata expiration check: 1 day, 4:01:06 ago on Tue 23 Nov 2021 11:32:05 AM CET.
[...]
Complete!
````

Vous devez comprendre toutes les commandes que vous tapez. En deux trois commandes c'est plié l'install.

## 3. Analyse

Avant de config étou, on va lancer à l'aveugle et inspecter ce qu'il se passe.

Commencez donc par démarrer le service NGINX :

```bash
$ sudo systemctl start nginx
$ sudo systemctl status nginx
```

🌞 **Analysez le service NGINX**

- avec une commande `ps`, déterminer sous quel utilisateur tourne le processus du service NGINX

````
[justin@localhost ~]$ sudo ps -aux
USER         PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
root        4762  0.0  0.2 119160  2164 ?        Ss   15:34   0:00 nginx: master process /usr/sbin/nginx
nginx       4763  0.0  0.9 151820  8068 ?        S    15:34   0:00 nginx: worker process
````

- avec une commande `ss`, déterminer derrière quel port écoute actuellement le serveur web

````
[justin@localhost html]$ sudo ss -ltpn
State  Recv-Q  Send-Q   Local Address:Port   Peer Address:Port Process
LISTEN 0       128            0.0.0.0:80          0.0.0.0:*     users:(("nginx",pid=4763,fd=8),("nginx",pid=4762,fd=8))
LISTEN 0       128               [::]:80             [::]:*     users:(("nginx",pid=4763,fd=9),("nginx",pid=4762,fd=9))
````

- en regardant la conf, déterminer dans quel dossier se trouve la racine web

````
 root         /usr/share/nginx/html;
````
- inspectez les fichiers de la racine web, et vérifier qu'ils sont bien accessibles en lecture par l'utilisateur qui lance le processus

````
[justin@localhost html]$ ls -l
total 20
-rw-r--r--. 1 root root 3332 Jun 10 11:09 404.html
-rw-r--r--. 1 root root 3404 Jun 10 11:09 50x.html
-rw-r--r--. 1 root root 3429 Jun 10 11:09 index.html
-rw-r--r--. 1 root root  368 Jun 10 11:09 nginx-logo.png
-rw-r--r--. 1 root root 1800 Jun 10 11:09 poweredby.png
````

## 4. Visite du service web

Et ça serait bien d'accéder au service non ? Bon je vous laisse pas dans le mur : spoiler alert, le service est actif, mais le firewall de Rocky bloque l'accès au service, on va donc devoir le configurer.

Il existe [un mémo dédié au réseau au réseau sous Rocky](../../cours/memos/rocky_network.md), vous trouverez le nécessaire là-bas pour le firewall.

🌞 **Configurez le firewall pour autoriser le trafic vers le service NGINX** (c'est du TCP ;) )

````
[justin@localhost html]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
````

🌞 **Tester le bon fonctionnement du service**

- avec votre navigateur sur VOTRE PC
  - ouvrez le navigateur vers l'URL : `http://<IP_VM>:<PORT>`
- vous pouvez aussi effectuer des requêtes HTTP depuis le terminal, plutôt qu'avec un navigateur
  - ça se fait avec la commande `curl`
  - et c'est ça que je veux dans le compte-rendu, pas de screen du navigateur :)

````
[justin@localhost html]$ curl http://10.200.1.10:80
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
````

## 5. Modif de la conf du serveur web

🌞 **Changer le port d'écoute**

- une simple ligne à modifier, vous me la montrerez dans le compte rendu
  - faites écouter NGINX sur le port 8080

````
 [justin@localhost html]$ cat /etc/nginx/nginx.conf
 [...]
 listen     8080 default_server;
 listen       [::]:8080 default_server;
````
- redémarrer le service pour que le changement prenne effet
  - `sudo systemctl restart nginx`
  - vérifiez qu'il tourne toujours avec un ptit `systemctl status nginx`

````
[justin@localhost html]$ sudo systemctl restart nginx
[justin@localhost html]$ systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
   Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; vendor preset: disabled)
   Active: active (running) since Wed 2021-11-24 19:44:48 CET; 4s ago
  Process: 5054 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)
  Process: 5052 ExecStartPre=/usr/sbin/nginx -t (code=exited, status=0/SUCCESS)
  Process: 5050 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, status=0/SUCCESS)
 Main PID: 5056 (nginx)
    Tasks: 2 (limit: 4946)
   Memory: 3.6M
   CGroup: /system.slice/nginx.service
           ├─5056 nginx: master process /usr/sbin/nginx
           └─5057 nginx: worker process

Nov 24 19:44:48 node1.tp4.linux systemd[1]: nginx.service: Succeeded.
Nov 24 19:44:48 node1.tp4.linux systemd[1]: Stopped The nginx HTTP and reverse proxy server.
Nov 24 19:44:48 node1.tp4.linux systemd[1]: Starting The nginx HTTP and reverse proxy server...
Nov 24 19:44:48 node1.tp4.linux nginx[5052]: nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
Nov 24 19:44:48 node1.tp4.linux nginx[5052]: nginx: configuration file /etc/nginx/nginx.conf test is successful
Nov 24 19:44:48 node1.tp4.linux systemd[1]: nginx.service: Failed to parse PID from file /run/nginx.pid: Invalid argume>
Nov 24 19:44:48 node1.tp4.linux systemd[1]: Started The nginx HTTP and reverse proxy server.
````
- prouvez-moi que le changement a pris effet avec une commande `ss`

````
[justin@localhost html]$ sudo ss -ltpn
State  Recv-Q  Send-Q   Local Address:Port   Peer Address:Port Process
LISTEN 0       128            0.0.0.0:8080        0.0.0.0:*     users:(("nginx",pid=5030,fd=8),("nginx",pid=5029,fd=8))
LISTEN 0       128               [::]:8080           [::]:*     users:(("nginx",pid=5030,fd=9),("nginx",pid=5029,fd=9))
````
- n'oubliez pas de fermer l'ancier port dans le firewall, et d'ouvrir le nouveau

````
[justin@localhost html]$ sudo firewall-cmd --remove-port=80/tcp --permanent
success
````
- prouvez avec une commande `curl` sur votre machine que vous pouvez désormais visiter le port 8080

````
[justin@localhost /]$ curl http://10.200.1.10:8080
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
````

---

🌞 **Changer l'utilisateur qui lance le service**

- pour ça, vous créerez vous-même un nouvel utilisateur sur le système : `web`
  - référez-vous au [mémo des commandes](../../cours/memos/commandes.md) pour la création d'utilisateur

````
[justin@localhost /]$ sudo useradd web
[sudo] password for justin:

````
  - l'utilisateur devra avoir un mot de passe, et un homedir défini explicitement à `/home/web`

````
[justin@localhost /]$ ls /home
justin  web
````
- un peu de conf à modifier dans le fichier de conf de NGINX pour définir le nouvel utilisateur en tant que celui qui lance le service

````
[justin@localhost /]$ sudo cat /etc/nginx/nginx.conf
# For more information on configuration, see:
#   * Official English Documentation: http://nginx.org/en/docs/
#   * Official Russian Documentation: http://nginx.org/ru/docs/

user web;
[...]
````

  - vous me montrerez la conf effectuée dans le compte-rendu
- n'oubliez pas de redémarrer le service pour que le changement prenne effet
- vous prouverez avec une commande `ps` que le service tourne bien sous ce nouveau utilisateur

````
[justin@localhost /]$ ps aux
USER         PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
web         5176  0.0  0.9 151820  7840 ?        S    20:20   0:00 nginx: worker process
````

---

🌞 **Changer l'emplacement de la racine Web**

- vous créerez un nouveau dossier : `/var/www/super_site_web`
  - avec un fichier  `/var/www/super_site_web/index.html` qui contient deux trois lignes de HTML, peu importe, un bon `<h1>toto</h1>` des familles, ça fera l'affaire
  - le dossier et tout son contenu doivent appartenir à `web`
- configurez NGINX pour qu'il utilise cette nouvelle racine web
  - vous me montrerez la conf effectuée dans le compte-rendu

````
 [justin@localhost var]$ cat /etc/nginx/nginx.conf
 [...]
 root         /var/www/super_site_web/;
````
- n'oubliez pas de redémarrer le service pour que le changement prenne effet
- prouvez avec un `curl` depuis votre hôte que vous accédez bien au nouveau site

````
[justin@localhost var]$ curl http://10.200.1.10:8080
<!DOCTYPE html>
<html>
<head><title>404 Not Found</title></head>
<body bgcolor="white">
<center><h1>404 Not Found Nan Jrigole encore</h1></center>
<hr><center>nginx/1.14.1</center>
</body>
</html>
````
